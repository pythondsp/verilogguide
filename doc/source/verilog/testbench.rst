.. _`ch_Testbenches`:

Testbenches
***********

.. raw:: latex

    \chapterquote{Nothing inside me belongs to me, all is Yours. In returning that which belongs to You, what does it cost to me?}{Kabeer}


Introduction
============


In previous chapters, we generated the simulation waveforms using modelsim, by providing the input signal values manually; if the number of input signals are very large and/or we have to perform simulation several times, then this process can be quite complex, time consuming and irritating. Suppose input is of 10 bit, and we want to test all the possible values of input i.e. :math:`2^{10}-1`, then it is impossible to do it manually. In such cases, testbenches are very useful; also, the tested designs are more reliable and prefer by the clients as well. Further, with the help of testbenches, we can generate results in the form of csv (comma separated file), which can be used by other softwares for further analysis e.g. Python, Excel and Matlab etc.  

**Since testbenches are used for simulation purpose only (not for synthesis), therefore full range of Verilog constructs can be used e.g. keywords 'for', 'display' and 'monitor' etc. can be used for writing testbenches.**

.. important:: 

    Modelsim-project is created in this chapter for simulations, which allows the relative path to the files with respect to project directory as shown in :numref:`sec_read_data_from_file`. Simulation can be run without creating the project, but we need to provide the full path of the files as shown in Line 25  of :numref:`verilog_read_file_ex`. 

Lastly, mixed modeling is not supported by Altera-Modelsim-starter version, i.e. Verilog designs with VHDL and vice-versa can not be compiled in this version of Modelsim. For mixed modeling, we can use Active-HDL software as discussed in Chapter :numref:`ch_overview`.


Testbench for combinational circuits
====================================

In this section, various testbenches for combinational circuits are shown, whereas testbenches for sequential circuits are discussed in next section. For simplicity of the codes and better understanding, a simple half adder circuit is tested using various simulation methods. 

Half adder
----------

:numref:`verilog_half_adder_v` shows the Verilog code for the half adder which is tested using different methods, 

.. literalinclude:: codes/Chapter-Testbenches/half_adder.v
    :language: verilog
    :Linenos:
    :caption: Half adder
    :name: verilog_half_adder_v


Testbench with 'initial block'
==============================

Note that, testbenches are written in separate Verilog files as shown in :numref:`verilog_half_adder_tb_v`. Simplest way to write a testbench, is to invoke the 'design for testing' in the testbench and provide all the input values inside the 'initial block', as explained below, 

**Explanation** :numref:`verilog_half_adder_tb_v`

    In this listing, a testbench with name 'half\_adder\_tb' is defined at Line 5. Note that, ports of the testbench is always empty i.e. no inputs or outputs are defined in the definition (see Line 5). Then 4 signals are defined i.e. a, b, sum and carry (Lines 7-8); these signals are then connected to actual half adder design using structural modeling (see Line 13). Lastly, different values are assigned to input signals e.g. 'a' and 'b' at lines 18 and 19 respectively. 
    
    .. note:: 
    
        'Initial block' is used at Line 15, which is executed only once, and terminated when the last line of the block executed i.e. Line 32. Hence, when we press run-all button in :numref:`fig_half_adder_tb`, then simulation terminated after 60 ns (i.e. does not run forever). 

    In Line 19, value of 'b' is 0, then it changes to '1' at Line 23, after a delay of 'period' defined at Line 20. The value of period is '20 (Line 11) \* timescale (Line 3) = 20 ns'. In this listing all the combinations of inputs are defined manually i.e. 00, 01, 10 and 11; and the results are shown in :numref:`fig_half_adder_tb`, also corresponding outputs i.e. sum and carry are shown in the figure. 
    
    .. note:: 

        To generate the waveform, first compile the 'half\_adder.v and then 'half\_adder\_tb.v' (or compile both the file simultaneously.). Then simulate the half\_adder\_tb.v file. Finally, click on 'run all' button (which will run the simulation to maximum time i.e. 80 ns)and then click then 'zoom full' button (to fit the waveform on the screen), as shown in :numref:`fig_half_adder_tb`.


    .. literalinclude:: codes/Chapter-Testbenches/half_adder_tb.v
        :language: verilog
        :Linenos:
        :caption: Simple testbench for half adder
        :name: verilog_half_adder_tb_v


    .. _fig_half_adder_tb:

    .. figure:: figures/testbench/half_adder_tb.jpg
        :scale: 100%
        :align: center
       
        Simulation results for :numref:`verilog_half_adder_tb_v`


.. _`sec_tb_with_process_statement`:

Testbench with 'always' block


In :numref:`verilog_half_adder_procedural_tb`, 'always' statement is used in the testbench; which includes the input values along with the corresponding output values.  If the specified outputs are not matched with the output generated by half-adder, then errors will be displayed. 

.. note:: 

    * In the testbenches, the 'always' statement can be written with or without the sensitivity list as shown in :numref:`verilog_half_adder_procedural_tb`.

    * Unlike 'initial' block, the 'always' block executes forever (if not terminated using 'stop' keyword). The statements inside the 'always' block execute sequentially; and after the execution of last statement, the execution begins again from the first statement of the 'always' block. 

**Explanation** :numref:`verilog_half_adder_procedural_tb`

    The listing is same as previous :numref:`verilog_half_adder_tb_v`, but 'always block' is used instead of 'initial block', therefore we can provide the sensitive list to the design (Line 28) and gain more control over the testing. A continuous clock is generated in Lines 19-26 by not defining the sensitive list to always-block (Line 19). This clock is used by Line 28. Also, some messages are also displayed if the outcome of the design does not match with the desire outcomes (Lines 35-36). In this way, we can find errors just by reading the terminal (see :numref:`fig_half_adder_process_error_tb`), instead of visualizing the whole waveform, which can be very difficult if the results are too long (see :numref:`fig_half_adder_process_tb`). **Also, Lines 35-36 can be added in 'initial-block' of :numref:`verilog_half_adder_tb_v` as well**.


    .. literalinclude:: codes/Chapter-Testbenches/half_adder_procedural_tb.v
        :language: verilog
        :Linenos:
        :caption: Testbench with procedural statement
        :name: verilog_half_adder_procedural_tb


    .. _fig_half_adder_process_error_tb:

    .. figure:: figures/testbench/half_adder_process_error_tb.jpg
        :scale: 100%
        :align: center
       
        Error generated by :numref:`verilog_half_adder_procedural_tb`

    .. _fig_half_adder_process_tb:

    .. figure:: figures/testbench/half_adder_process_tb.jpg
        :scale: 100%
        :align: center
       
        Simulation results for :numref:`verilog_half_adder_procedural_tb`


.. _`sec_read_data_from_file`:

Read data from file
-------------------


In this section, data is read from file 'read\_file\_ex.txt' and displayed in simulation results. Date stored in the file is shown in :numref:`fig_read_file_table_ex`. 

.. _fig_read_file_table_ex:

.. figure:: figures/testbench/read_file_table_ex.jpg
    :scale: 100%
    :align: center
   
    Data in file  'read\_file\_ex.txt'


**Explanation** :numref:`verilog_read_file_ex`

    In the listing, 'integer i (Line 16)' is used in the 'for loop (Line 28)' to read all the lines of file 'read\_file\_ex.txt'. Data can be saved in 'binary' or 'hexadecimal format'. Since data is saved in 'binary format', therefor 'readmemb' (Line 23) is used to read it. For 'hexadecimal format', we need to use keyword 'readmemh'. Read comments for further details of the listing. Data read by the listing is displayed in :numref:`fig_read_file_ex`.  


    .. literalinclude:: codes/Chapter-Testbenches/read_file_ex.v
        :language: verilog
        :Linenos:
        :caption: Read data from file
        :name: verilog_read_file_ex


    .. _fig_read_file_ex:

    .. figure:: figures/testbench/read_file_ex.jpg
        :scale: 100%
        :align: center
       
        Simulation results of :numref:`verilog_read_file_ex`


Write data to file
------------------

In this part, different types of values are defined in :numref:`verilog_write_file_ex_v` and then stored in the file. 

**Explanation** :numref:`verilog_write_file_ex_v`

    To write the data to the file, first we need to define an 'integer' as shown in Line 14, which will work as buffer for open-file (see Line 28). Then data is written in the files using 'fdisplay' command, and rest of the code is same as :numref:`verilog_read_file_ex`.  

    .. literalinclude:: codes/Chapter-Testbenches/write_file_ex.v
        :language: verilog
        :Linenos:
        :caption: Write data to file
        :name: verilog_write_file_ex_v


    .. _fig_write_file_table_ex:

    .. figure:: figures/testbench/write_file_table_ex.jpg
        :scale: 100%
        :align: center
       
        Data in file  'write\_file\_ex.txt'


Testbench for sequential designs
================================


In previous sections, we read the lines from one file and then saved those line in other file using 'for loop'. Also, we saw use of 'initial' and 'always' blocks. In this section, we will combine all the techniques together to save the results of Mod-M counter, which is an example of 'sequential design'. The Mod-m counter is discussed in :numref:`verilog_modMCounter`. Testbench for this listing is shown in :numref:`verilog_modMCounter_tb` and the waveforms are illustrated in :numref:`fig_monitor_waveform`.

**Explanation** :numref:`verilog_modMCounter_tb`

    In the testbench following operations are performed, 

    * Simulation data is saved on the terminal and saved in a file. Note that, **'monitor/fmonitor'**  keyword can be used in the **'initial' block**, which can track the changes in the signals as shown in Lines 98-108 and the output is shown in :numref:`fig_monitor_error`. In the figure we can see that the 'error' message is displayed two time (but the error is at one place only), as 'monitor' command checks for the transition in the signal as well. 
        
    * If we want to track only final results, then **'display/fdisplay'** command can be used inside the **always block** as shown in Lines 110-124 (Uncomment these lines and comment Lines 98-108). Output is show in :numref:`fig_error_neg_edge_csv`. **Note that, it is very easy to look for errors in the terminal/csv file as compare to finding it in the waveforms.** 
        
    * Simulation is stopped after a fixed number of clock-cycle using 'if statement' at Line 76-84. This method can be extended to stop the simulation after certain condition is reached.
        
    .. note:: 
    
        Notice the use of 'negedge' in the code at Lines 89 and 117, to compare the result saved in file 'mod\_m\_counter\_desired.txt' (:numref:`fig_content_mod_m_desire`) with result obtained by 'modMCounter.v' (Line 32). For more details, please read the comments in the code. 
    
    
    .. _fig_monitor_waveform:

    .. figure:: figures/testbench/monitor_waveform.jpg
        :scale: 100%
        :align: center
       
        Waveform for :numref:`verilog_modMCounter_tb`

    
    .. _fig_monitor_error:

    .. figure:: figures/testbench/monitor_error.jpg
        :scale: 100%
        :align: center
       
        Data displayed using 'initial block' and 'monitor' (Lines 98-108 of :numref:`verilog_modMCounter_tb`)

    
    .. _fig_error_neg_edge_csv:

    .. figure:: figures/testbench/error_neg_edge_csv.jpg
        :scale: 100%
        :align: center
       
        Data saved in .csv file using 'always block' and 'fdisplay' (Lines 110-124 of :numref:`verilog_modMCounter_tb`)

    
    .. _fig_content_mod_m_desire:

    .. figure:: figures/testbench/content_mod_m_desire.jpg
        :scale: 100%
        :align: center
       
        Content of file 'mod\_m\_counter\_desired.txt'


    .. literalinclude:: codes/Chapter-Testbenches/modMCounter_tb.v
        :language: verilog
        :Linenos:
        :caption: Save data of simulation
        :name: verilog_modMCounter_tb

Conclusion
==========

In this chapter, we learn to write testbenches with different styles for combinational circuits and sequential circuits. We saw the methods by which inputs can be read from the file and the outputs can be written in the file. Simulation results and expected results are compared and saved in the csv file and displayed as simulation waveforms; which demonstrated that locating the errors in csv files is easier than the simulation waveforms.