Interface
*********


Introduction
============

The interface allows to create a group of signals, which can be used as ports in designs. 


Define and use interface
========================

* In :numref:`sv_or_ex`, an interface is defined in Lines 3-9, which contains signals a, b, c and c_2 in it. Then, in module or_ex, these signals are used as shown in Lines 19-20 and 22.  The simulation results are shown in :numref:`fig_or_ex`.

.. code-block:: systemverilog
    :linenos: 
    :emphasize-lines: 3-9, 19-20, 22
    :caption: Define and use interface
    :name: sv_or_ex

    // interface_ex.sv

    interface logic_gate_if;

    logic[3:0] a, b;
    logic[5:0] c;
    logic[5:0] c_2; // increment c by 2

    endinterface


    module or_ex(
        input logic[3:0] a, b,
        output logic[5:0] sum, sum_new
    );

    logic_gate_if lg ();  // import all signals from interface

    assign lg.a = a;
    assign lg.b = b;
    assign sum = a+b;
    assign sum_new = lg.a + lg.b + 2;  // increment sum by 2

    endmodule


.. _`fig_or_ex`:

.. figure:: figures/interface/interface1.png

   Simulation results for :numref:`sv_or_ex`


* Also, we can perform some assigments in the the interfaces as shown in Lines 9-10 of :numref:`sv_or_ex2`. Also, these newly assigned values are assigned to output at Lines 23-24. The simulation results are same as in :numref:`fig_or_ex`.

.. warning:: 

    Do following, after making the changes in the code (otherwise changes will not be shown in the simulation),

    * Close the previous simulation
    * Then, right click on the 'Run simulation->Reset Behaviour simulation'
    * Then, run the Behaviour-simulation again


.. code-block:: systemverilog
    :linenos: 
    :emphasize-lines: 9-10, 23-24
    :caption: Assigment in interface
    :name: sv_or_ex2

    // interface_ex.sv

    interface logic_gate_if;

    logic[3:0] a, b;
    logic[5:0] c;
    logic[5:0] c_2; // increment c by 2

    assign c = a + b;
    assign c_2 = c + 2;
    endinterface


    module or_ex(
        input logic[3:0] a, b,
        output logic[5:0] sum, sum_2
    );

    logic_gate_if lg ();  // import all signals from interface

    assign lg.a = a;
    assign lg.b = b;
    assign sum = lg.c;
    assign sum_2 = lg.c_2; // increment sum by 2

    endmodule
