.. _`ch_FSM`:

Finite state machine
********************

.. raw:: latex

    \chapterquote{Darkness can not be dispelled by darkness, but by brightness. Hatred can not be overcome by hatred, but by loving kindness.}{Buddha}


Introduction
============

In previous chapters, we saw various examples of the combinational circuits and sequential circuits. In combinational circuits, the output depends on the current values of inputs only; whereas in sequential circuits, the output depends on the current values of the inputs along with the previously stored information. In the other words, storage elements, e.g. flip flogs or registers, are required for sequential circuits. 

The information stored in the these elements can be seen as the states of the system. If a system transits between finite number of such internal states, then finite state machines (FSM) can be used to design the system. In this chapter, various finite state machines along with the examples are discussed. Further, please see the SystemVerilog-designs in  :numref:`Chapter %s <ch_SystemVerilogSyn>`, which provides the better ways for creating the FSM designs as compared to Verilog. 


.. _`sec_MealyMooreDesign`:

Comparison: Mealy and Moore designs
===================================

\section{}\label{}
FMS design is known as Moore design if the output of the system depends only on the states (see :numref:`fig_MooreEdgeDetector`); whereas it is known as Mealy design if the output depends on the states and external inputs (see :numref:`fig_MealyEdgeDetector`). Further, a system may contain both types of designs simultaneously.

.. note:: 


    Following are the differences in Mealy and Moore design, 

    * In Moore machine, the outputs depend on states only, therefore it is '**synchronous machine**' and the output is available after 1 clock cycle as shown in :numref:`fig_edgeDetectorWave`. Whereas, in Mealy machine output depends on states along with external inputs; and the output is available as soon as the input is changed therefore it is '**asynchronous machine**' (See :numref:`fig_Mealy_MooreGlitchFree` for more details).
    * Mealy machine requires fewer number of states as compared to Moore machine as shown in :numref:`sec_MealyMooreDect`.     
    * Moore machine should be preferred for the designs, where glitches (see :numref:`sec_glitches`) are not the problem in the systems. 
    * Mealy machines are good for synchronous systems which requires 'delay-free and glitch-free' system (See example in Section :numref:`sec_exampleRegularMMGlitchFree`), but careful design is required for asynchronous systems. Therefore, Mealy machine can be complex as compare to Moore machine. 


Example: Rising edge detector
=============================

Rising edge detector generates a tick for the duration of one clock cycle, whenever input signal changes from 0 to 1. In this section, state diagrams of rising edge detector for Mealy and Moore designs are shown. Then rising edge detector is implemented using Verilog code. Also, outputs of these two designs are compared. 


.. _`sec_MealyMooreDect`:

State diagrams: Mealy and Moore design
--------------------------------------

:numref:`fig_MealyEdgeDetector` and :numref:`fig_MooreEdgeDetector` are the state diagrams for Mealy and Moore designs respectively. In :numref:`fig_MealyEdgeDetector`, the output of the system is set to 1, whenever the system is in the state 'zero' and value of the input signal 'level' is 1; i.e. output depends on both the state and the input. Whereas in :numref:`fig_MooreEdgeDetector`, the output is set to 1 whenever the system is in the state 'edge' i.e. output depends only on the state of the system. 


.. _fig_MooreEdgeDetector:

.. figure:: figures/fsm/MooreEdgeDetector.jpg
    :scale: 50%
    :align: center
   
    State diagrams for Edge detector : Moore Design

.. _fig_MealyEdgeDetector:

.. figure:: figures/fsm/MealyEdgeDetector.jpg
    :scale: 50%
    :align: center
   
    State diagrams for Edge detector : Mealy Design


Implementation
--------------

Both Mealy and Moore designs are implemented in :numref:`verilog_edgeDetector`. The listing can be seen as two parts i.e. Mealy design (Lines 37-55) and Moore design (Lines 57-80). Please read the comments for complete understanding of the code. The simulation waveforms i.e. :numref:`fig_edgeDetectorWave` are discussed in next section. 

.. literalinclude:: codes/Chapter-Finite-state-machines/edgeDetector.v
    :language: verilog
    :Linenos:
    :caption: Edge detector: Mealy and Moore designs
    :name: verilog_edgeDetector

Outputs comparison
------------------


In :numref:`fig_edgeDetectorWave`, it can be seen that output-tick of Mealy detector is generated as soon as the 'level' goes to 1, whereas Moore design generate the tick after 1 clock cycle. These two ticks are shown with the help of the two red cursors in the figure. Since, output of Mealy design is immediately available therefore it is preferred for synchronous designs. 

.. _fig_edgeDetectorWave:

.. figure:: figures/fsm/edgeDetectorWave.jpg
    :scale: 100%
    :align: center
   
    Simulation waveforms of rising edge detector in :numref:`verilog_edgeDetector`


Visual verification
-------------------

:numref:`verilog_edgeDetector_VisualTest` can be used to verify the results on the FPGA board. Here, clock with 1 Hz frequency is used in line 19, which is defined in :numref:`verilog_clockTick`. After loading the design on FPGA board, we can observe on LEDs that the output of Moore design displayed after  Mealy design, with a delay of 1 second.  

.. literalinclude:: codes/Chapter-Finite-state-machines/edgeDetector_VisualTest.v
    :language: verilog
    :Linenos:
    :caption: Visual verification of edge detector
    :name: verilog_edgeDetector_VisualTest


.. _`sec_glitches`:

Glitches
========

Glitches are the short duration pulses which are generated in the combinational circuits. These are generated when more than two inputs change their values simultaneously. Glitches can be categorized as 'static glitches' and 'dynamic glitches'. Static glitches are further divided into two groups i.e. 'static-0' and 'static-1'. 'Static-0' glitch is the glitch which occurs in logic '0' signal i.e. **one short pulse** i.e. 'high-pulse (logic-1)' appears in logic-0 signal (and the signal settles down). Dynamic glitch is the glitch in which **multiple short pulses** appear before the signal settles down. 

.. note:: 

    Most of the times, the glitches are not the problem in the design. Glitches create problem when it occur in the outputs, which are used as clock for the other circuits. In this case, glitches will trigger the next circuits, which will result in incorrect outputs. In such cases, it is very important to remove these glitches. In this section, the glitches are shown for three cases. Since, clocks are used in synchronous designs, therefore Section :numref:`sec_glitchInsSync` is of our main interest.


Combinational design in asynchronous circuit
--------------------------------------------

:numref:`tbl_glitch_table` shows the truth-table for :math:`2 \times 1` multiplexer and corresponding Karnaugh map is shown in :numref:`fig_glitch_sol`. Note that, the glitches occurs in the circuit, when we exclude the 'red part' of the solution from the :numref:`fig_glitch_sol`, which results in minimum-gate solution, but at the same time the solution is disjoint. To remove the glitch, we can add the prime-implicant in red-part as well. This solution is good, if there are few such gates are required; however if the number of inputs are very high, whose values are changing simultaneously then this solution is not practical, as we need to add large number of gates.


.. _tbl_glitch_table:

.. figure:: figures/fsm/glitch_table.jpg
    :scale: 70%
    :align: center
   
    Truth table of :math:`2 \times 1` Multiplexer


.. _fig_glitch_sol:

.. figure:: figures/fsm/glitch_sol.jpg
    :scale: 70%
    :align: center
   
    Reason for glitches and solution

.. _fig_glitches:

.. figure:: figures/fsm/glitches.jpg
    :scale: 100%
    :align: center
       
    Glitches (see disjoint lines in 'z') in design in :numref:`verilog_glitchEx`

.. literalinclude:: codes/Chapter-Finite-state-machines/glitchEx.v
    :language: verilog
    :Linenos:
    :caption: Glitches in multiplexer
    :name: verilog_glitchEx



Unfixable Glitch
----------------

:numref:`verilog_manchester_code` is another example of glitches in the design as shown in :numref:`fig_manchesterGlitch`. Here, glitches are continuous i.e. these are occurring at every change in signal 'din'. Such glitches are removed by using D-flip-flop as shown in Section :numref:`sec_glitchInsSync`. **Since the output of Manchester code depends on both edges of clock (i.e. half of the output changes on +ve edge and other half changes at -ve edge), therefore such glitches are unfixable; as in Verilog both edges can not be connected to one D flip flop.**


.. _fig_manchesterGlitch:

.. figure:: figures/fsm/manchesterGlitch.jpg
    :scale: 100%
    :align: center
   
    Glitches in :numref:`verilog_manchester_code`


.. literalinclude:: codes/Chapter-Finite-state-machines/manchester_code.v
    :language: verilog
    :Linenos:
    :caption: Glitches in Manchester coding
    :name: verilog_manchester_code


.. _`sec_glitchInsSync`:

Combinational design in synchronous circuit
-------------------------------------------

Combination designs in sequential circuits were discussed in :numref:`fig_combSeqBlock`. The output of these combination designs can depend on states only, or on the states along with external inputs. The former is known as Moore design and latter is known as Mealy design as discussed in Section :numref:`sec_MealyMooreDesign`. Since, the sequential designs are sensitive to edge of the clock, therefore the glitches can occur only at the edge of the clock. Hence, the glitches at the edge can be removed by sending the output signal through the D flip flop, as shown in :numref:`fig_combSeqBlockGlitchFree`. Various Verilog templates for sequential designs are shown in Section :numref:`sec_MooreTemplates` and :numref:`sec_MealyTemplates`. 

.. _fig_combSeqBlockGlitchFree:

.. figure:: figures/fsm/combSeqBlockGlitchFree.jpg
    :scale: 70%
    :align: center
   
    Glitch-free sequential design using D flip flop


.. _`sec_MooreTemplates`:

Moore architecture and Verilog templates
========================================

:numref:`fig_combSeqBlockGlitchFree` shows the different block for the sequential design. In this figure, we have three blocks i.e. 'sequential logic', 'combinational logic' and 'glitch removal block'. In this section, we will define three process-statements to implemented these blocks (see :numref:`verilog_moore_regular_template2`). Further, 'combinational logic block' contains two different logics i.e. 'next-state' and 'output'. Therefore, this block can be implemented using two different block, which will result in four process-statements (see :numref:`verilog_moore_regular_template`). 

Moore and Mealy machines can be divided into three categories i.e. 'regular', 'timed' and 'recursive'. The differences in these categories are shown in :numref:`fig_regular_mcn`, :numref:`fig_timed_mcn` and :numref:`fig_recursive_mcn` for Moore machine. In this section, we will see different Verilog templates for these categories. Note that, the design may be the combinations of these three categories, and we need to select the correct template according to the need. Further, the examples of these templates are shown in Section :numref:`sec_VerilogtemplateExample`.



.. _fig_regular_mcn:

.. figure:: figures/fsm/regular.jpg
    :scale: 70%
    :align: center
   
    Regular Moore machine

.. _fig_timed_mcn:

.. figure:: figures/fsm/timed.jpg
    :scale: 70%
    :align: center
   
    Timed Moore machine : next state depends on time as well

.. _fig_recursive_mcn:

.. figure:: figures/fsm/recursive.jpg
    :scale: 70%
    :align: center
   
    Recursive Moore machine : output 'z' depends on output i.e. feedback required


Regular machine
---------------

Please see the :numref:`fig_regular_mcn` and note the following points about regular Moore machine, 

* Output depends only on the states, therefore **no 'if statement'** is required in the process-statement. For example, in Lines 85-87 of :numref:`verilog_moore_regular_template`, the outputs (Lines 86-87) are defined inside the 's0' (Line 86). 
* Next-state depends on current-state and and  **current external inputs**. For example, the 'state\_next' at Line 49 of :numref:`verilog_moore_regular_template` is defined inside 'if statement' (Line 48) which depends on current input. Further, this 'if statement' is defined inside the state 's0' (Line 47). Hence the next state depends on current state and current external input.    

.. note:: 

    In regular Moore machine, 

    * Outputs depend on current external inputs. 
    * Next states depend on current states and current external inputs.


.. literalinclude:: codes/Chapter-Finite-state-machines/moore_regular_template.v
    :language: verilog
    :Linenos:
    :caption: Verilog template for regular Moore FSM : separate 'next\_state' and 'output' logic
    :name: verilog_moore_regular_template



:numref:`verilog_moore_regular_template2` is same as :numref:`verilog_moore_regular_template`, but the ouput-logic and next-state logic are combined in one process block. 


.. literalinclude:: codes/Chapter-Finite-state-machines/moore_regular_template2.v
    :language: verilog
    :Linenos:
    :caption: Verilog template for regular Moore FSM : combined 'next\_state' and 'output' logic
    :name: verilog_moore_regular_template2


Timed machine
-------------

If the state of the design changes after certain duration (see :numref:`fig_timed_mcn`), then we need to add the timer in the Verilog design which are created in :numref:`verilog_moore_regular_template` and :numref:`verilog_moore_regular_template`. For this, we need to add one more process-block which performs following actions, 

* **Zero the timer** : The value of the timer is set to zero, whenever the state of the system changes. 
* **Stop the timer** : Value of the timer is incremented till the predefined 'maximum value' is reached and then it should be stopped incrementing. Further, it's value should **not** be set to zero until state is changed. 


.. note::

    In timed Moore machine, 

    * Outputs depend on current external inputs. 
    * Next states depend on time along with current states and current external inputs.

Template for timed Moore machine is shown in :numref:`verilog_moore_timed_template`, which is exactly same as :numref:`verilog_moore_regular_template2` except with following changes, 

* Timer related constants are added at Line 22-27.
* An 'always' block is added to stop and zero the timer (Lines 44-54). 
* Finally, timer related conditions are included for next-state logic e.g. Lines 64 and 67 etc.

.. literalinclude:: codes/Chapter-Finite-state-machines/moore_timed_template.v
    :language: verilog
    :Linenos:
    :caption: Verilog template timed Moore FSM : separate 'next\_state' and 'output' logic
    :name: verilog_moore_timed_template

Recursive machine
-----------------

In recursive machine, the outputs are fed back as input to the system (see :numref:`fig_recursive_mcn`). Hence, we need additional process-statement which can store the outputs which are fed back to combinational block of sequential design, as shown in :numref:`verilog_moore_recursive_template`. The listing is same as :numref:`verilog_moore_timed_template` except certain signals and process block are defined to feedback the output to combination logic; i.e. Lines 29-31 contain the signals (feedback registers) which are required to be feedback the outputs. Here, '\_next' and '\_reg' are used in these lines, where 'next' value is fed back as 'reg' in the next clock cycle inside the 'always' statement which is defined in Lines 63-75. Lastly, 'feedback registers' are also used to calculate the next-state inside the 'if statement' e.g. Lines 91 and 96. Also, the value of feedback registers are updated inside these 'if statements' e.g. Lines 93 and 102. 


.. note::

    In recursive Moore machine, 

    * Outputs depend on current external inputs. Also, values in the feedback registers are used as outputs.
    * Next states depend current states, current external input, current internal inputs (i.e. previous outputs feedback as inputs to system) and time (optional).

.. literalinclude:: codes/Chapter-Finite-state-machines/moore_recursive_template.v
    :language: verilog
    :Linenos:
    :caption: Verilog template recursive Moore FSM : separate 'next\_state' and 'output' logic
    :name: verilog_moore_recursive_template


.. _`sec_MealyTemplates`:

Mealy architecture and Verilog templates
========================================

Template for Mealy architecture is similar to Moore architecture. The minor changes are required as outputs depend on current input as well, as discussed in this section.

Regular machine
---------------

In Mealy machines, the output is the function of current input and states, therefore the output will also defined inside the if-statements (Lines 49-50 etc.). Rest of the code is same as :numref:`verilog_moore_regular_template2`. 

.. literalinclude:: codes/Chapter-Finite-state-machines/mealy_regular_template.v
    :language: verilog
    :Linenos:
    :caption: Verilog template for regular Mealy FSM : combined 'next\_state' and 'output' logic
    :name: verilog_mealy_regular_template


Timed machine
-------------

:numref:`verilog_mealy_timed_template` contains timer related changes in :numref:`verilog_mealy_regular_template`. See description of :numref:`verilog_moore_timed_template` for more details. 


.. literalinclude:: codes/Chapter-Finite-state-machines/mealy_timed_template.v
    :language: verilog
    :Linenos:
    :caption: Verilog template for timed Mealy FSM : combined 'next\_state' and 'output' logic
    :name: verilog_mealy_timed_template


Recursive machine
-----------------

:numref:`verilog_mealy_recursive_template` contains recursive-design related changes in :numref:`verilog_mealy_timed_template`. See description of :numref:`verilog_moore_recursive_template` for more details. 

.. literalinclude:: codes/Chapter-Finite-state-machines/mealy_recursive_template.v
    :language: verilog
    :Linenos:
    :caption: Verilog template for recursive Mealy FSM : combined 'next\_state' and 'output' logic
    :name: verilog_mealy_recursive_template


.. _`sec_VerilogtemplateExample`:

Examples
========

.. _`sec_exampleRegularMMGlitchFree`:

Regular Machine : Glitch-free Mealy and Moore design
----------------------------------------------------

In this section, a non-overlapping sequence detector is implemented to show the differences between Mealy and Moore machines. :numref:`verilog_sequence_detector` implements the 'sequence detector' which detects the sequence '110'; and corresponding state-diagrams are shown in :numref:`subfig_Moore_seq_det_state` and :numref:`subfig_Mealy_seq_det_state`. The RTL view generated by the listing is shown in :numref:`fig_Mealy_Moore_RTL_glitchfree`, where two D-FF are added to remove the glitches from Moore and Mealy model. Also, in the figure, if we click on the state machines, then we can see the implemented state-diagrams e.g. if we click on 'state\_reg\_mealy' then the state-diagram in :numref:`fig_Altera_Mealy_seq_det_state` will be displayed, which is exactly same as :numref:`subfig_Mealy_seq_det_state`. 


Further, the testbench for the listing is shown in :numref:`verilog_sequence_detector_tb`, whose results are illustrated in :numref:`fig_Mealy_MooreGlitchFree`. Please note the following points in :numref:`fig_Mealy_MooreGlitchFree`,

* Mealy machines are asynchronous as the output changes as soon as the input changes. It does not wait for the next cycle. 
* If the output of the Mealy machine is delayed, then glitch will be removed and the output will be same as the Moore output (Note that, there is no glitch in this system. This example shows how to implement the D-FF to remove glitch in the system (if exists)). 
* Glitch-free Moore output is delayed by one clock cycle. 
* If glitch is not a problem, then we should use Moore machine, because it is synchronous in nature. But, if glitch is problem and we do not want to delay the output then Mealy machines should be used. 


.. _subfig_Moore_seq_det_state:

.. figure:: figures/fsm/Moore_seq_det_state.jpg
    :scale: 50%
    :align: center
   
    Non-overlap sequence detector '110' : Moore design

.. _subfig_Mealy_seq_det_state:

.. figure:: figures/fsm/Mealy_seq_det_state.jpg
    :scale: 50%
    :align: center
   
    Non-overlap sequence detector '110' : Mealy design


.. _fig_Altera_Mealy_seq_det_state:

.. figure:: figures/fsm/Altera_Mealy_seq_det_state.jpg
    :scale: 70%
    :align: center
   
    State diagram generated by Quartus for Mealy machine in :numref:`verilog_sequence_detector`


.. _fig_Mealy_Moore_RTL_glitchfree:

.. figure:: figures/fsm/Mealy_Moore_RTL_glitchfree.jpg
    :scale: 100%
    :align: center
   
    RTL view generated by :numref:`verilog_sequence_detector`


.. _fig_Mealy_MooreGlitchFree:

.. figure:: figures/fsm/Mealy_MooreGlitchFree.jpg
    :scale: 100%
    :align: center
   
    Mealy and Moore machine output for :numref:`verilog_sequence_detector_tb`

.. literalinclude:: codes/Chapter-Finite-state-machines/sequence_detector.v
    :language: verilog
    :Linenos:
    :caption: Glitch removal using D-FF
    :name: verilog_sequence_detector

.. literalinclude:: codes/Chapter-Finite-state-machines/sequence_detector_tb.v
    :language: verilog
    :Linenos:
    :caption: Testbench for :numref:`verilog_sequence_detector`
    :name: verilog_sequence_detector_tb



Timed machine: programmable square wave
----------------------------------------

:numref:`verilog_square_wave_ex` generates the square wave using Moore machine, whose 'on' and 'off' time is programmable (see Lines 6 and 7) according to state-diagram in :numref:`fig_program_s_wave`.  The simulation waveform of the listing are shown in :numref:`fig_square_wave_ex`.

.. _fig_program_s_wave:

.. figure:: figures/fsm/program_s_wave.jpg
    :scale: 50%
    :align: center
   
    State diagram for programmable square-wave generator


.. _fig_square_wave_ex:

.. figure:: figures/fsm/square_wave_ex.jpg
    :scale: 100%
    :align: center
   
    Simulation waveform of :numref:`verilog_square_wave_ex`


.. literalinclude:: codes/Chapter-Finite-state-machines/square_wave_ex.v
    :language: verilog
    :Linenos:
    :caption: Square wave generator
    :name: verilog_square_wave_ex


Recursive Machine : Mod-m counter
---------------------------------

:numref:`verilog_counterEx` implements the Mod-m counter using Moore machine, whose state-diagram is shown in :numref:`fig_counter_state_diagram`. Machine is recursive because the output signal 'count\_moore\_reg' (Line 50) is used as input to the system (Line 32). The simulation waveform of the listing are shown in :numref:`fig_counterEx`

.. _fig_counter_state_diagram:

.. figure:: figures/fsm/counter_state_diagram.jpg
    :scale: 50%
    :align: center
   
    State diagram for Mod-m counter

.. _fig_counterEx:

.. figure:: figures/fsm/counterEx.jpg
    :scale: 100%
    :align: center
   
    Simulation waveform of :numref:`verilog_counterEx`


.. note:: 

    It is not good to implement every design using FSM e.g. :numref:`verilog_counterEx` can be easily implement without FSM as shown in :numref:`verilog_modMCounter`. Please see the Section :numref:`sec_whentouseFsm` for understading the correct usage of FSM design. 


.. literalinclude:: codes/Chapter-Finite-state-machines/counterEx.v
    :language: verilog
    :Linenos:
    :caption: Mod-m Counter
    :name: verilog_counterEx


.. _`sec_whentouseFsm`:

When to use FSM design
======================

We saw in previous sections that, once we have the state diagram for the FSM design, then the Verilog design is a straightforward process. But, it is important to understand the correct conditions for using the FSM, otherwise the circuit will become complicated unnecessary. 


* We should not use the FSM diagram, if there is only 'one loop' with 'zero or one control input'. 'Counter' is a good example for this. A 10-bit counter has 10 states with no control input (i.e. it is free running counter). This can be easily implemented without using FSM as shown in :numref:`verilog_binaryCounter`. If we implement it with FSM, then we need 10 states; and the code and corresponding design will become very large. 
    
* If required, then FSM can be use for 'one loop' with 'two or more control inputs'. 
    
* FSM design should be used in the cases where there are very large number of loops (especially connected loops) along with two or more controlling inputs. 
    

Conclusion
==========

In this chapter, Mealy and Moore designs are discussed. Also, 'edge detector' is implemented using Mealy and Moore designs. This example shows that Mealy design requires fewer states than Moore design. Further, Mealy design generates the output tick as soon as the rising edge is detected; whereas Moore design generates the output tick after a delay of one clock cycle. Therefore, Mealy designs are preferred for synchronous designs. 

 